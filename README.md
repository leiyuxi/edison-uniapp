# edison-uniapp

#### 介绍
分享一些日常开发中封装的uniapp组件与页面模板

#### 如何使用

1.下载源码

2.使用Hbuilder导入

3.运行到小程序或手机


#### 预览

![小程序码](static/images/qrcode.jpg)





 **----基础组件----** 

单元格  es-cell-list/es-cell-item

异形选项卡  es-tabs

上传组件   es-upload




 **----业务组件----** 

 条件筛选栏  es-filter-bar

城市选择器  es-city-select




 **----页面模板----** 

电子名片 es-business-card

APP登录模板 es-app-login


 **更多组件持续更新中，请关注作者公众号，第一时间获取组件源码** 

![输入图片说明](static/images/wx-public.jpg)





#### 部分页面展示


<img src="static/images/%E7%94%B5%E5%AD%90%E5%90%8D%E7%89%87.jpg" width="300">

<img src="static/images/1.jpg" width="300">

<img src="static/images/2.jpg" width="300">

<img src="static/images/3.jpg" width="300">

<img src="static/images/4.jpg" width="300">

<img src="static/images/5.jpg" width="300">



