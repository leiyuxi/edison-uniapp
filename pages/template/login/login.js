
// 登录方法封装

const login = {
	/**
	* @name 小程序-微信登录
	* @param requestUrl 微信登录接口地址
	*/
	weappLogin: function(requestUrl="login/weappLogin") {
		return new Promise((resolve, reject) => {
			uni.login({
				success: function(res) {
					if (res.code) {
						uni.getUserInfo({
							success: function(userInfo) {
								let formData = {
									code: res.code,
									encryptedData: userInfo.encryptedData,
									iv: userInfo.iv
								}
								
								//1.此处直接模拟登录成功
								resolve(formData)
								//2.请根据实际业务需求封装post请求 
								// this.$http.post(requestUrl, formData).then(res => {
								// 	resolve(res)
								// }).catch((res) => {
								// 	reject(res);
								// });
							}
						});
					} else {
						reject('登录失败');
					}
				},
				fail: function(err) {
					reject(err);
				}
			});
			
		
		})
	},
	/**
	* @name APP-微信登录
	* @param requestUrl 微信登录接口地址
	*/
	wxLogin: function(requestUrl="login/wxLogin") {
		return new Promise((resolve, reject) => {
			uni.login({
				provider: 'weixin',
				success: function(loginRes) {
					if (!loginRes.authResult.unionid) {
						login.wxLogin();
						return false;
					}
					// 获取用户信息
					uni.getUserInfo({
						provider: 'weixin',
						success: function(infoRes) {
							let formData = {
								accessToken: loginRes.authResult
									.access_token,
								openId: loginRes.authResult.openid,
								unionId: loginRes.authResult.unionid,
								nickName: infoRes.userInfo.nickName,
								avatarUrl: infoRes.userInfo.avatarUrl,
								gender: infoRes.userInfo.gender,
								province: infoRes.userInfo.province,
								city: infoRes.userInfo.city
							}
							
							//1.此处直接模拟登录成功
							resolve(formData)
							//2.请根据实际业务需求封装post请求 
							// this.$http.post(requestUrl, formData).then(res => {
							// 	resolve(res)
							// }).catch((res) => {
							// 	reject(res);
							// });
							
						},
						fail: function(err) {
							reject(err);
						}
					});

				},
				fail: function(err) {
					reject(err);
				}
			});
		})
	},
	/**
	* @name APP-qq登录
	* @param requestUrl qq登录接口地址
	*/
	qqLogin(requestUrl="login/qqLogin"){
		return new Promise((resolve, reject) => {
			uni.login({
			    provider: 'qq',
			    success: function (loginRes) {
					let formData={
						openId:loginRes.authResult.openid,
						accessToken:loginRes.authResult.access_token,
						expiresIn:loginRes.authResult.expires_in
					}
					
					//1.此处直接模拟登录成功
					resolve(formData)
					//2.请根据实际业务需求封装post请求 
					// this.$http.post(requestUrl, formData).then(res => {
					// 	resolve(res)
					// }).catch((res) => {
					// 	reject(res);
					// });
					
			    },
			    fail: function (err) {
					reject(err);
			    }
			});
		})
	},
	/**
	* @name APP-苹果登录
	* @param requestUrl 苹果登录接口地址
	*/
	appleLogin: function(requestUrl="login/appleLogin") {
		return new Promise((resolve, reject) => {
			let appleOauth = null;
			plus.oauth.getServices(function(services) {
				for (var i in services) {
					var service = services[i];
					// 获取苹果授权登录对象，苹果授权登录id 为 'apple' iOS13以下系统，不会返回苹果登录对应的 service    
					if (service.id == 'apple') {
						appleOauth = service;
						break;
					}
				}
				appleOauth.logout();
				appleOauth.login(function(oauth) {
					// 授权成功，苹果授权返回的信息在 oauth.target.appleInfo 中    
					let appleInfo = oauth.target.appleInfo;
					let formData = {
						authorizationCode: appleInfo.authorizationCode,
						identityToken: appleInfo.identityToken,
						appleId: appleInfo.user,
						nickName: appleInfo.fullName.nickName || ""
					}
					
					//1.此处直接模拟登录成功
					resolve(formData)
					//2.请根据实际业务需求封装post请求 
					// this.$http.post(requestUrl, formData).then(res => {
					// 	resolve(res)
					// }).catch((res) => {
					// 	reject(res);
					// });
					
				}, function(err) {
					reject(err);
					
				}, {
					// 默认只会请求用户名字信息，如需请求用户邮箱信息，需要设置 scope: 'email'    
					scope: 'email'
				})
			}, function(err) {
				// 获取 services 失败  
				reject(err);
			})
		})
	}
}

export default login